# See LICENSE file for copyright and license details

dosage: main.c
	c99 -D_DEFAULT_SOURCE -Wall -pedantic -g main.c -lm -o dosage

clean:
	rm dosage

/* See LICENSE file for copyright and license details */
#include <ctype.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct measure {
	double val;
	char *units;
};
struct dose_info {
	struct measure dose;      /* How much */
	struct measure interval;  /* Time between doses */
	struct measure half_life; /* How fast does it go away */
};
struct time_info {
	struct measure epsilon;     /* time between samples */
	struct measure time_length; /* total time length */
};
struct input {
	struct dose_info d;
	struct time_info t;
	char *preferred_time_unit;
};
double consentration(const struct dose_info *d, double time)
{
	double t = time;
	double k = d->dose.val;
	double T = d->interval.val;
	double lambda = log(2)/d->half_life.val; /* yes */
	double ret = 0;
	for(int i = 0; i <= floor(t/T); ++i) {
		ret += k * exp(-lambda * t) * exp(lambda * T * i);//TODO: verify this
	}
	return ret;
}
void fancy_print(const struct input *in, double scale)
{
	double epsilon = in->t.epsilon.val;
	double time_length = in->t.time_length.val;
	for(double t = 0; t < time_length; t += epsilon) {
		if(t > floor(t) - epsilon && t < floor(t) + epsilon)
			printf("day %.0f\n", floor(t));
		int i = 0;
		double R = consentration(&in->d, t);
		while(i++ < (R - 1) * scale)
				printf("-");
		printf("| %lf\n", R);
	}
}
void print_input(FILE *f, const struct input *i)
{
	char *fmt = "%s%lf %s\n";
	fprintf(f, fmt, "dose        ", i->d.dose.val, i->d.dose.units);
	fprintf(f, fmt, "interval    ", i->d.interval.val, i->d.interval.units);
	fprintf(f, fmt, "halflife    ", i->d.half_life.val, i->d.half_life.units);
	fprintf(f, fmt, "epsilon     ", i->t.epsilon.val, i->t.epsilon.units);
	fprintf(f, fmt, "timelength  ", i->t.time_length.val, i->t.time_length.units);

}
void f_print(FILE *f, const struct input *i)
{
	double epsilon = i->t.epsilon.val;
	double time_length = i->t.time_length.val;
	print_input(f, i);
	for(double t = 0; t < time_length; t += epsilon)
		fprintf(f, "%lf, %.10lf\n", t, consentration(&i->d, t));
}
int strtom(struct measure *m, char *s)
{
	m->val = strtod(s, &m->units);
	if(s == m->units)
		return 1;
	while(isspace(*m->units))
		++m->units;
	if(!m->units)
		return 1;
	return 0;
}
void print_extra(FILE *f, const struct input *i)
{
	fprintf(f,
	"\n"
	"Estimated plasma concentration every %lf %s for %lf %s \n"
	"Output units: %s\n",
	i->t.epsilon.val, i->t.epsilon.units,
	i->t.time_length.val, i->t.time_length.units,
	i->d.dose.units);
}
int convert_time(struct measure *m, char *units)
{
	char *u[]           = {"sec", "min", "hour", "day", "week", ""};
	double conversion[] = { 1,     60,    60,     24,    7,      0};
	int i;
	for(i = 0; *u[i] != '\0'; ++i) {
		if(!strncmp(m->units, u[i], strlen(u[i]))) {
			break;
		}
	}
	int j;
	for(j = 0; *u[j] != '\0'; ++j) {
		if(!strncmp(units, u[j], strlen(u[j]))) {
			break;
		}
	}
	int len = 0;
	while(*u[++len] != '\0')
		;
	if(len == i || len == j){
		return 1;
	}
	if( i == j) {
		return 0;
	} else if(i < j) {
		double *conv = &conversion[1];
		while(i != j) {
			m->val /= conv[i++];
		}
	} else {
		double *conv = &conversion[0];
		while(i != j) {
			m->val *= conv[i--];
		}
	}
	m->units = units;
	return 0;
}
int verify_args(struct input *in)
//TODO: Find a preferred time scale and convert everything to that
//      instead of asking the user to do the conversion for us
{
	char *t = in->t.time_length.units;
	char *e = in->t.epsilon.units;
	char *i = in->d.interval.units;
	char *h = in->d.half_life.units;
	char *units = t; /* choose something else if this is not good */
	int err = 0;
	if(!t) {
		fprintf(stderr, "missing time\n");
		err = 1;
	}
	if(!e) {
		fprintf(stderr, "missing epsilon\n");
		err = 1;
	}
	if(!i) {
		fprintf(stderr, "missing dose interval\n");
		err = 1;
	}
	if(!h) {
		fprintf(stderr, "missing half life\n");
		err = 1;
	}
	if(err)
		return 2;
	convert_time(&in->t.time_length, units);
	convert_time(&in->t.epsilon, units);
	convert_time(&in->d.interval, units);
	convert_time(&in->d.half_life, units);

	t = in->t.time_length.units;
	e = in->t.epsilon.units;
	i = in->d.interval.units;
	h = in->d.half_life.units;

	if(!(strcmp(e, units) && strcmp(i, units) && strcmp(h, units)))
		return 0;

	fprintf(stderr, "note: options -i, -r, -e and -t must have the same units\n");
	return 2;
}
char *pname;
void usage(void) {
	fprintf(stderr,
	"usage: %s [-h] [-d dose] [-i interval] [-r halflife] [-e epsilon] [-t time]\n"
	"\n"
	"-d dose\n"
	"-i interval\n"
	"-r half life / elimination rate\n"
	"-e epsilon\n"
	"-t over how long\n"
	"-h this help text\n"
	"-p pretty print\n"
	"-m machine print\n"
	"\n", pname);
}
int main(int argc, char *argv[])
{
	pname = argv[0];
	struct input in = {0};
	char c = 0;
	int fpretty = 0;
	int fmachine_readable = 0;
	while((c = getopt(argc, argv, "d:i:r:e:t:hpm")) != -1) {
		switch(c) {
		case 'p':
			fpretty = 1;
			break;
		case 'm':
			fmachine_readable = 1;
			break;
		case 'd':
			if(strtom(&in.d.dose,optarg))
				goto opt_err;
			break;
		case 'i':
			if(strtom(&in.d.interval,optarg))
				goto opt_err;
			break;
		case 'r':
			if(strtom(&in.d.half_life,optarg))
				goto opt_err;
			break;
		case 'e':
			if(strtom(&in.t.epsilon,optarg))
				goto opt_err;
			break;
		case 't':
			if(strtom(&in.t.time_length,optarg))
				goto opt_err;
			break;
		default:
			goto opt_err;
		}
	}
	if(verify_args(&in)) {
		goto err;
	}
	if(fpretty && fmachine_readable) {
		fprintf(stderr, "-p and -m options are mutually exclusive");
		return 2;
	}
	if(fmachine_readable) {
		f_print(stdout, &in);
	} else {
		print_extra(stdout, &in);
	}
	if(fpretty) {
		fancy_print(&in, 10);
	}

	return 0;
opt_err:
	if(optarg){
		fprintf(stderr,"did not recognize '%s' as a measure try '7days' or '3g'\n", optarg);
	}
err:
	usage();
	return 2;
}
